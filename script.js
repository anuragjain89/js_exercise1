var User = function (name, age) {
  this.age = age;
  this.name = name;
};

User.prototype.compare = function (other_user) {
  var message;
  if (this.age > other_user.age) {
    message = this.name + ' is older than ' + other_user.name;
  } else if (this.age < other_user.age) {
    message = other_user.name + ' is older than ' + this.name;
  } else {
    message = this.name + ' is as old as ' + other_user.name;
  }
  console.log(message);
};

var user1 = new User('John', 25);
var user2 = new User('Mary', 27);
var user3 = new User('Donna', 27);

user1.compare(user2);
user3.compare(user2);